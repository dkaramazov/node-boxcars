var express = require('express');
var router = express.Router();
var data = require("../data.json");

router.get('/', (req, res) => {
    printRollChart();
    res.json(data);
});

router.get('/sanity', (req, res) => {
    res.send({ html: generateHTMLChart() });
});

router.get('/regions/:country', (req, res) => {
    var country = req.params.country.toUpperCase();
    var regions = data[country].regions;
    res.send({ regions });
});

router.get('/cities/:country', (req, res) => {
    var country = req.params.country.toUpperCase();
    var cities = data[country].cities;
    res.send({ cities });
});

router.get('/payoff/:country/:origin/:destination', (req, res) => {
    var country = req.params.country.toUpperCase();
    var origin = data[country].cities.indexOf(req.params.origin);
    var destination = data[country].cities.indexOf(req.params.destination);
    var value = data[country].payoff[origin][destination] || 0;
    res.send({ route: `${req.params.origin} to ${req.params.destination}`, value });
});

router.get('/getRegion/:country/:odd/:white', (req, res) => {
    var country = req.params.country.toUpperCase();
    var odd = parseInt(req.params.odd, 10);
    var white = parseInt(req.params.white, 10);
    var index = data[country].region_rollchart[odd][white];
    var name = data[country].regions[index];
    res.send({ index, name });
});

router.get('/getCity/:country/:region/:odd/:white', (req, res, next) => {
    var country = req.params.country.toUpperCase();
    var odd = parseInt(req.params.odd, 10);
    var white = parseInt(req.params.white, 10);
    var index = data[country].city_rollchart[req.params.region][odd][white];
    var name = data[country].cities[index];
    res.send({ index, name });
});

router.get('/rollCity/:country/:region', (req, res) => {
    var country = req.params.country.toUpperCase();
    var region = parseInt(req.params.region, 10);
    var r = rollDice();
    var index = data[country].city_rollchart[region][r[0]][r[1] + r[2]];
    var name = data[country].cities[index];
    res.send({ index, name });
});

router.get('/rollRegion/:country', (req, res) => {
    var country = req.params.country.toUpperCase();
    var r = rollDice();
    var index = data[country].region_rollchart[r[0]][r[1] + r[2]];
    var name = data[country].regions[index];
    res.send({ index, name });
});

// Rolls 3 dice, 1 d2 and 2 d6.
// NOTE: These rolls start at 0, not 1.
var rollDice = function () {
    return [
        Math.floor(Math.random() * 2),
        Math.floor(Math.random() * 6),
        Math.floor(Math.random() * 6)
    ];
};

function printRollChart() {
    var padLeft = function (str, size) {
        return ("                                      " + str).slice(-1 * size);
    };
    var padRight = function (str, size) {
        return (str + "                                      ").substr(0, size - 1);
    };

    for (mapName in data) {
        console.log("map name: " + mapName);
        m = data[mapName];
        out = [
            "Region Roll Chart",
            "-----------------------------------------------",
            "ODD                    | EVEN"
        ];
        for (var i = 0; i <= 10; i++) {
            s = padLeft(i + 2, 2) + ": " + padRight(m.regions[m.region_rollchart[1][i]], 20);
            s += "|";
            s += padLeft(i + 2, 2) + ": " + padRight(m.regions[m.region_rollchart[0][i]], 20);
            out.push(s);
        }
        console.log(out.join("\n"));

        for (r = 0; r < m.regions.length; r++) {

            out = [
                m.regions[r] + " Roll Chart",
                "-----------------------------------------------",
                "ODD                    | EVEN"
            ];
            for (var i = 0; i <= 10; i++) {
                s = padLeft(i + 2, 2) + ": " + padRight(m.cities[m.city_rollchart[r][1][i]], 20);
                s += "|";
                s += padLeft(i + 2, 2) + ": " + padRight(m.cities[m.city_rollchart[r][0][i]], 20);
                out.push(s);
            }
            console.log(out.join("\n"));
        }
    }
}

function generateHTMLChart() {
    let html = `<div class="boxcar-data-chart" style="width:320px;margin:auto;">\n`;

    for (mapName in data) {
        html += `<h1>Map Name: ${mapName}</h2>\n`;
        m = data[mapName];
        html += `<h2>Region Roll Chart</h2>\n`;
        html += `<table>
                    <thead>
                        <th>ODD</th>
                        <th>EVEN</th>
                    </thead>
                    <tbody>
                `;
        for (var i = 0; i <= 10; i++) {
            html += `<tr>
                        <td>${i + 2}</td>
                        <td>${m.regions[m.region_rollchart[1][i]]}</td>
                    </tr>
                    <tr>
                        <td>${i + 2}</td>
                        <td>${m.regions[m.region_rollchart[0][i]]}</td>
                    </tr>`;
        }
        html += `</tbody>\n</table>`;

        for (r = 0; r < m.regions.length; r++) {
            html += `\n<h2>${m.regions[r]} Roll Chart</h2>\n`;
            html += `<table>
                        <thead>
                            <th>ODD</th>
                            <th>EVEN</th>
                        </thead>
                        <tbody>`;
            for (var i = 0; i <= 10; i++) {
                html += `<tr>
                            <td>${i + 2}</td>
                            <td>${m.cities[m.city_rollchart[r][1][i]]}</td>
                        </tr>
                        <tr>
                            <td>${i + 2}</td>
                            <td>${m.cities[m.city_rollchart[r][0][i]]}</td>
                        </tr>`
            }
            html += `</tbody>\n</table>`;
        }
    }
    html += `\n</div>`;
    return html;
}

module.exports = router;